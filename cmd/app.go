package cmd

import (
	"fmt"
	"os"
)

type Command struct {
	Name string
	Desc string
	Func func(args []string)
}

type MainCMD struct {
	cmds []*Command
}

func (m *MainCMD) Add(cmd *Command) {
	m.cmds = append(m.cmds, cmd)
}

func (m *MainCMD) Execute() {
	if len(os.Args) < 2 {
		m.usage()
	}
	// split command & sub-args
	cmdName := os.Args[1]
	var args []string
	if len(os.Args) > 2 {
		args = os.Args[2:]
	}

	for _, c := range m.cmds {
		if c.Name == cmdName {
			c.Func(args)
			goto SHUTDOWN
		}
	}

	m.usage()

	SHUTDOWN:
}

func (m *MainCMD) usage() {
	s := os.Args[0] + ":\n"
	s += "Usage:\n"

	for _, c := range m.cmds {
		s += fmt.Sprintf("  %-10s %s\n", c.Name, c.Desc)
	}

	os.Exit(2)
}

