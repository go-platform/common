package tests

import (
	"fmt"
	"sync/atomic"
	"time"

	app "gitlab.com/go-platform/common"
)

type nullService struct {
	RunFunc func() error
	inRun   int32
}

func (n *nullService) InitFlags()       {}
func (n *nullService) Configure() error { return nil }
func (n *nullService) Stop()            {}
func (n *nullService) Run() error {
	atomic.StoreInt32(&n.inRun, 1)
	if n.RunFunc != nil {
		return n.RunFunc()
	} else {
		for {
			time.Sleep(time.Second)
		}
	}
	return nil
}
func (n *nullService) Cleanup() {}

func (n *nullService) WaitRun() {
	for atomic.LoadInt32(&n.inRun) == 0 {
		time.Sleep(time.Millisecond)
	}
}

type producerService struct {
	nullService
	log app.Logger
	ch  chan string
}

func newproducerService(log app.Logger, ch chan string) app.RunnableService {
	return &producerService{
		log: log,
		ch:  ch,
	}
}

func (c *producerService) Run() error {
	for i := 0; i < 5; i++ {
		c.ch <- fmt.Sprintf("message %d", i)
	}
	close(c.ch)
	return nil
}

type consumeService struct {
	nullService
	log      app.Logger
	ch       chan string
	chQuit   chan bool
	chQuitOk chan bool
}

func newConsumeService(log app.Logger, ch chan string) app.RunnableService {
	return &consumeService{
		log:      log,
		ch:       ch,
		chQuit:   make(chan bool, 1),
		chQuitOk: make(chan bool, 1),
	}
}

func (c *consumeService) Run() error {
	for {
		select {
		case s, ok := <-c.ch:
			if !ok {
				goto SHUTDOWN
			}
			c.log.Info(s)
		case <-c.chQuit:
			goto SHUTDOWN
		}
	}

SHUTDOWN:
	c.chQuitOk <- true
	return nil
}

func (c *consumeService) Stop() {
	c.chQuit <- true
	<-c.chQuitOk
}

func executeApp(app app.Application) func() {
	go app.Run()
	return func() {
		<-app.Shutdown()
	}
}
