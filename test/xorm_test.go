package tests


import (
	"fmt"
	"os"
	"strconv"
	"testing"

	app "gitlab.com/go-platform/common"
	"gitlab.com/go-platform/common/log"
	"gitlab.com/go-platform/common/xorm"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

func doSimpleSqlTest(t *testing.T, app app.Application) {
	sqls := xorm.NewSqlService(&xorm.SqlConfig{
		App: app,
	})
	app.RegService(sqls)

	nullS := &nullService{}
	app.RegMainService(nullS)

	defer executeApp(app)()

	nullS.WaitRun()

	db := sqls.DB()
	err := db.Ping()
	if err != nil {
		t.Fatal(err)
	}

	row , err := db.QueryString("SELECT (1+1) as count")

	var n int

	n, err = strconv.Atoi(row[0]["count"])

	if err != nil {
		t.Fatal(err)
	}

	if n != 2 {
		t.Fatal("Result must be 2")
	}
}

func TestMySql(t *testing.T) {
	sqlUrl := fmt.Sprintf("mysql://%s:%s@tcp(%s)/%s",
		os.Getenv("TEST_SQL_USER"),
		os.Getenv("TEST_SQL_PASS"),
		os.Getenv("TEST_SQL_HOST"),
		os.Getenv("TEST_SQL_DB"))

	fmt.Println("sqlUrl" , sqlUrl)
	app := app.NewApp(&app.AppConfig{
		Args: []string{
			"sql-uri", sqlUrl,
		},
		LogConfig: &log.LoggerConfig{
			DefaultLevel: "warn",
		},
		UseNewFlagSet: true,
	})

	doSimpleSqlTest(t, app)
}
