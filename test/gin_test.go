package tests

import (
	"fmt"
	"log"
	"net/http"
	"testing"
	"github.com/gin-gonic/gin"
	 "gitlab.com/go-platform/common"
	ginServer "gitlab.com/go-platform/common/gin"
	cs "gitlab.com/go-platform/common/consul"
	)

func registerHandlers(r *gin.Engine) {
	r.GET("/ping", pingHandler)
}

func pingHandler(c *gin.Context) {
	c.String(200, "pong")
}

func ginRequest(port int) {
	url := fmt.Sprintf("http://127.0.0.1:%d/ping", port)

	resp, err := http.DefaultClient.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(resp)
}

func TestGin(t *testing.T) {
	app := common.NewApp(&common.AppConfig{
		Args: []string{
			"-port", "3001",
			"-addr", "127.0.0.1",
			"-gin-mode", "release",
		},
		UseNewFlagSet: true,
	})

	cnf := ginServer.Config{
		App:         app,
		Consul: 	cs.NewNullConsul(),
		ServiceName: "test-gin",
		RegFunc:     registerHandlers,
	}
	gSvc, _ := ginServer.New(&cnf)

	app.RegMainService(gSvc)

	defer executeApp(app)()

	ginRequest(gSvc.Port())
}
