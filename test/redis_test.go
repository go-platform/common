package tests


import (
	"testing"

	"github.com/gomodule/redigo/redis"

	app "gitlab.com/go-platform/common"
	"gitlab.com/go-platform/common/log"
	redisGo "gitlab.com/go-platform/common/redis"
)

type redigoTest struct {
	nullService
	t *testing.T

	sred redisGo.RedigoService
}

func (m *redigoTest) Run() error {
	var err error

	c := m.sred.Get()
	defer c.Close()

	if err = c.Err(); err != nil {
		m.t.Fatal(err)
	}

	_, err = c.Do("SET", "hello", "world")
	if err != nil {
		m.t.Fatal(err)
	}
	defer c.Do("DEL", "hello")

	s, err := redis.String(c.Do("GET", "hello"))
	if err != nil {
		m.t.Fatal(err)
	}

	if s != "world" {
		m.t.Fatal("Can't get setted-value")
	}

	return nil
}
func TestRedigo(t *testing.T) {
	app := app.NewApp(&app.AppConfig{
		Args: []string{},
		LogConfig: &log.LoggerConfig{
			DefaultLevel: "error",
		},
		UseNewFlagSet: true,
	})

	red := redisGo.NewRedigo(&redisGo.RedigoConfig{
		App: app,
	})
	app.RegService(red)

	app.RegMainService(&redigoTest{t: t, sred: red})

	app.Run()
}

