package gin
import (
	"net"
	"net/http"
	"time"
)

type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(3 * time.Minute)
	return tc, nil
}

type httpServer struct {
	http.Server
}

func (srv *httpServer) Serve(lis net.Listener) error {
	return srv.Server.Serve(tcpKeepAliveListener{lis.(*net.TCPListener)})
}

func (srv *httpServer) ServeTLS(lis net.Listener, certFile, keyFile string) error {
	return srv.Server.ServeTLS(tcpKeepAliveListener{lis.(*net.TCPListener)}, certFile, keyFile)
}

