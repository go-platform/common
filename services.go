package common

type Service interface {
	InitFlags()
	Configure() error
	Cleanup()
}

type ReloadableService interface {
	Service
	Reload()
}

// Runnable service
type RunnableService interface {
	Service

	// Start main logic
	Run() error

	// Use to stop from another goroutine
	Stop()
}
