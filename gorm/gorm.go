package gorm

import (
	"flag"

	"github.com/jinzhu/gorm"
	app "gitlab.com/go-platform/common"
	"gitlab.com/go-platform/common/sql"
)

var (
	haveInitGlobalFlag bool
	gormDebug          bool
)

func initGlobalFlag() {
	if haveInitGlobalFlag {
		return
	}

	flag.BoolVar(&gormDebug, "gorm-debug", false, "Turn on gorm.Debug (global)")
	haveInitGlobalFlag = true
}

type GormSqlService interface {
	app.Service
	DriverName() string

	GormDB() *gorm.DB
}

func NewSqlService(config *sql.SqlConfig) GormSqlService {
	return &gormSqlService{
		SqlService: sql.NewSqlService(config),
	}
}

type gormSqlService struct {
	sql.SqlService
	gormDb *gorm.DB
}

func (g *gormSqlService) InitFlags() {
	g.SqlService.InitFlags()
	initGlobalFlag()
}

func (g *gormSqlService) GormDB() *gorm.DB {
	if g.gormDb == nil {
		g.gormDb, _ = gorm.Open(g.DriverName(), g.DB())
	}
	return g.gormDb
}

type GormSqlManagerService interface {
	sql.SqlManagerService

	GormDB(dbId sql.DatabaseID) (*gorm.DB, error)
}

func NewSqlManagerService(config *sql.ManagerServiceConfig) GormSqlManagerService {
	return &gormSqlManagerServiceImpl{
		SqlManagerService: sql.NewSqlManagerService(config),

		gPool: make(map[sql.DatabaseID]*gorm.DB),
	}
}

type gormSqlManagerServiceImpl struct {
	sql.SqlManagerService

	gPool map[sql.DatabaseID]*gorm.DB
}

func (g *gormSqlManagerServiceImpl) InitFlags() {
	g.SqlManagerService.InitFlags()
	initGlobalFlag()
}

func (g *gormSqlManagerServiceImpl) GormDB(dbId sql.DatabaseID) (*gorm.DB, error) {
	if gormDb, ok := g.gPool[dbId]; ok {
		return gormDb, nil
	}

	drv, err := g.DriverName(dbId)
	if err != nil {
		return nil, err
	}

	db, err := g.DB(dbId)
	if err != nil {
		return nil, err
	}

	gormDb, err := gorm.Open(drv, db)
	if err == nil {
		if gormDebug {
			gormDb = gormDb.Debug()
		}
		g.gPool[dbId] = gormDb
	}

	return gormDb, err
}
